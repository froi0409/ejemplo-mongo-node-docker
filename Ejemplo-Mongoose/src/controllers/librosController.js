const Libro = require('../models/Libro');

const agregarLibro =  async (req, res) => {
    const insertarLibro = new Libro({
        nombre: req.body.nombre,
        autor: req.body.autor,
        etiquetas: req.body.etiquetas,
        fecha: new Date(req.body.fecha)
    });

    const confirmacion = await insertarLibro.save();
    res.json(confirmacion);
};

const obtenerLibros = async (req, res) => {
    const busqueda = await Libro.find();
    res.json(busqueda);
}

const actualizarLibro = async (req, res) => {
    const update = await Libro.updateMany({ nombre: req.body.nombre_inicial }, { $set: { nombre: req.body.nombre_final } });
    res.json(update);
}

const eliminarLibro = async (req, res) => {
    const remove = await Libro.deleteMany({ nombre: req.body.nombre });
    res.json(remove);
}

module.exports = {
    agregarLibro,
    obtenerLibros,
    actualizarLibro,
    eliminarLibro
}
