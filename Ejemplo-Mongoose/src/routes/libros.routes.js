const express = require('express');
const Libro = require('../models/Libro');
const librosController = require('../controllers/librosController');

const router = express.Router();

router.post('/agregarLibro', librosController.agregarLibro);

router.get('/obtenerLibros', librosController.obtenerLibros);

router.put('/actualizarLibro', librosController.actualizarLibro);

router.delete('/eliminarLibro', librosController.eliminarLibro);

module.exports = router;
