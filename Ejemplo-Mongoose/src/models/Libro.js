const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const model = mongoose.model;

const libroSchema = new Schema({
    nombre: String,
    autor: String,
    etiquetas: Array,
    fecha: Date
}, {
    versionKey: false
});

module.exports = model('Libros', libroSchema);
