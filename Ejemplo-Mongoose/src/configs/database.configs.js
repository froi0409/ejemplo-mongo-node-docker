const mongoose = require('mongoose');

const host = 'db';
const port = 27017;
const database = 'libreria';

async function start() {
    try {
        const db = await mongoose.connect(`mongodb://${host}:${port}/${database}`, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            family: 4
        });
        console.log(`Conexion realizada con éxito a base de datos: ${database}`);
    } catch (error) {
        console.error(error);
    }
}

module.exports = {
    start
}
