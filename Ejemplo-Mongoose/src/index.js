const express = require('express');
const { start } = require('./configs/database.configs');
const LibrosRoutes = require('./routes/libros.routes');

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

start();
const port = 4000;

app.get('/', (req, res) => {
    res.send('Inicio');
})

app.use('/api', LibrosRoutes);

// Iniciamos el Servidor
const server = app.listen(port, () => {
    console.log(`Servidor escuchando en el puerto ${port}`);
});
